### Stage 1: Build
FROM node:12-alpine as build
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build --prod

### Stage 2: Run
FROM nginx:1.17.1-alpine as prod-stage
COPY --from=build /app/dist/appmovie /usr/share/nginx/html
EXPOSE 80
CMD ["nginx","-g","daemon off;"]


