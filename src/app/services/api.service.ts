import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public myUrl: string = CONFIG.myUrl;
  public urlApi: string = CONFIG.urlApi;
  public urlApiAuth: string = CONFIG.urlApiAuth;
  public key: string = CONFIG.key;
  public langEs: string = CONFIG.langEs;

  constructor(public http: HttpClient) { }

  getTopRated(type: string, option: string, page: number) {
    let url = `${this.urlApi}/${type}/${option}${this.key}${this.langEs}&page=${page}`;
    return this.http.get(url);
  }

  getMovie(id: number, type: string){
    let url = `${this.urlApi}/${type}/${id}${this.key}${this.langEs}`;
    return this.http.get(url);
  }

  getToken() {
    let url = `${this.urlApi}/authentication/token/new${this.key}`;
    return this.http.get(url);
  }

  postCreateSession(token: string) {
    let url = `${this.urlApiAuth}/${token}?redirect_to=${this.myUrl}`;
    window.open(url, "_self");
    return true;
  }

  postNewSession(token: string) {
    let url = `${this.urlApi}/authentication/session/new${this.key}`;
    return this.http.post(url, { request_token: token });
  }

  getFavourites(sessionId: string, page: number) {
    let url = `${this.urlApi}/account/{account_id}/favorite/movies${this.key}&session_id=${sessionId}&page=${page}`;
    return this.http.get(url);
  }

  postFavourite(sessionId: string, mediaType: string, mediaId: number, status: boolean) {
    let url = `${this.urlApi}/account/{account_id}/favorite${this.key}&session_id=${sessionId}`;
    return this.http.post(url, { "media_type": mediaType, "media_id": mediaId, "favorite": status });
  }

  getDetailsUser(sessionId: string){
    let url = `${this.urlApi}/account${this.key}&session_id=${sessionId}`;
    return this.http.get(url);
  }

}
