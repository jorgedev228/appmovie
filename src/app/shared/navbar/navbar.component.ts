import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public nameUser: string;
  public session: boolean = false;
  public token: string;
  public session_id: string;

  constructor(public _apiService: ApiService, public _router: Router) {
    this.token = localStorage.getItem('token');
    this.session_id = localStorage.getItem('session_id');
    localStorage.getItem('username') ? this.nameUser = localStorage.getItem('username') : this.nameUser = 'Menú';
  }

  ngOnInit(): void {
  }

  getToken() {
    this._apiService.getToken()
      .subscribe((resp: any) => {
        localStorage.setItem('token', resp.request_token);
        this.token = resp.request_token;
        this.authAccount();
      }, error => {
        Swal.fire('Lo sentimos', 'Se ha producido un error.', 'error');
        console.log(error);
      });
  }

  authAccount() {
    this._apiService.postCreateSession(this.token);
  }

  newSession() {
    if (localStorage.getItem('username')) {
      Swal.fire('¡Aviso!', `Ya ha iniciado sesión`, 'info');
      return;
    }
    this._apiService.postNewSession(this.token)
      .subscribe((resp: any) => {
        localStorage.setItem('session_id', resp.session_id);
        this.getDetails(resp.session_id);
      }, error => {
        Swal.fire('Lo sentimos', 'Se ha producido un error.', 'error');
        console.log(error);
      });
  }

  getDetails(sessionId: string) {
    this._apiService.getDetailsUser(sessionId)
      .subscribe((resp: any) => {
        localStorage.setItem('username', resp.username);
        this.nameUser = resp.username;
        this._router.navigate(['favourites']);
      }, error => {
        console.log(error);
      });
  }

  toFavourites() {
    if (!localStorage.getItem('username')) {
      Swal.fire('¡Aviso!', `Debes haber iniciado sesión para ver tu lista.`, 'info');
      return;
    }
    this._router.navigate(['favourites']);
  }

  logout() {
    localStorage.clear();
    window.location.href = '/';
  }

}
