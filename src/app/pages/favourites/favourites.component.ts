import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { CONFIG } from '../../config/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {

  public title: string = 'Mis favoritos';
  public sessionId: string;
  public favourites: any[] = [];
  public imageUrl: string = CONFIG.urlImage;
  public totalPages: number;
  public loading: boolean = true;

  constructor(public _apiService: ApiService, public _router: Router) {
    this.sessionId = localStorage.getItem('session_id');
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(page = 1) {
    this._apiService.getFavourites(this.sessionId, page)
      .subscribe((resp: any) => {
        this.favourites = this.favourites.concat(resp.results);
        if (resp.total_pages > resp.page) {
          this.getData(page + 1);
        }
        if (resp.total_pages == resp.page) {
          this.favourites.sort((b, a) => a.vote_average - b.vote_average);
        }
        this.loading = false;
      }, error => {
        console.log(error);
      });
  }

  deleteFav(id: number) {
    this._apiService.postFavourite(this.sessionId, 'movie', id, false)
      .subscribe((resp: any) => {
        Swal.fire('¡Correcto!', 'La película fue eliminada correctamente.', 'success');
        this.favourites = this.favourites.filter(x => { return x.id != id; })
      }, error => {
        Swal.fire('Lo sentimos', 'Se ha producido un error al eliminar la película.', 'error');
        console.log(error);
      });
  }

  showFav(id: string) {
    this._router.navigate(['movie', id]);
  }

}
