import { Component, OnInit } from '@angular/core';
import { CONFIG } from '../../config/config';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  public imageUrl: string = CONFIG.urlImage;
  public id: number;
  public movie = {} as any;
  public loading: boolean = true;
  public sessionId: string;

  constructor(public _apiService: ApiService, public _router: Router, public _activatedRoute: ActivatedRoute) {
    _activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.sessionId = localStorage.getItem('session_id');
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this._apiService.getMovie(this.id, 'movie')
      .subscribe((resp: any) => {
        this.movie = resp;
        this.loading = false;
      }, error => {
        Swal.fire('Lo sentimos', 'Se ha producido un error al cargar la película.', 'error');
        console.log(error);
      });
  }

  addFavourite() {
    if (!localStorage.getItem('username')) {
      Swal.fire('¡Aviso!', `Inicie sesión para añadir a su lista de favoritos.`, 'info');
      return;
    }
    this._apiService.postFavourite(this.sessionId, 'movie', this.id, true)
      .subscribe((resp: any) => {
        Swal.fire('¡Correcto!', `"${this.movie.title}" fue añadadia correctamente a su lista de favoritos.`, 'success');
      }, error => {
        Swal.fire('Lo sentimos', 'Se ha producido un error al añadirla a su lista de favoritos.', 'error');
        console.log(error);
      });
  }

}
