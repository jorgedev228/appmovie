import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { CONFIG } from '../../config/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public title: string = 'Top 100';
  public currentPage: number = 1;
  public movies: any[] = [];
  public imageUrl: string = CONFIG.urlImage;
  public sessionId: string;
  public loading: boolean = true;

  constructor(public _apiService: ApiService, public _router: Router) { }

  ngOnInit(): void {
    this.getData();
    this.sessionId = localStorage.getItem('session_id');
  }

  getData() {
    this._apiService.getTopRated('movie', 'top_rated', this.currentPage)
      .subscribe((resp: any) => {
        this.movies = resp.results;
        this.loading = false;
      }, error => {
        Swal.fire('Lo sentimos', 'Se ha producido un error al cargar la lista.', 'error');
        console.log(error);
      });
  }

  showDescription(movie: any) {
    this._router.navigate(['/movie', movie.id]);
  }

  addFavourite(movie: any) {
    if (!localStorage.getItem('username')) {
      Swal.fire('¡Aviso!', `Inicie sesión para añadir a su lista de favoritos.`, 'info');
      return;
    }
    this._apiService.postFavourite(this.sessionId, 'movie', movie.id, true)
      .subscribe((resp: any) => {
        Swal.fire('¡Correcto!', `"${movie.title}" fue añadadia correctamente a su lista de favoritos.`, 'success');
      }, error => {
        Swal.fire('Lo sentimos', 'Se ha producido un error al añadirla a su lista de favoritos.', 'error');
        console.log(error);
      });
  }

  pagination(event: any) {
    this.currentPage = parseInt(event.path[0].innerText);
    this.getData();
  }

}
