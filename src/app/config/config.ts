export const CONFIG = {
    urlApi: 'https://api.themoviedb.org/3',
    urlApiAuth: 'https://www.themoviedb.org/authenticate',
    urlImage: 'https://image.tmdb.org/t/p/w500',
    key: '?api_key=74208251ba29dcc433f3deceea49f3f5',
    langEs: '&language=es',
    myUrl: 'http://localhost:4200'
};